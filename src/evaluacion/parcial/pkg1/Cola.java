
package evaluacion.parcial.pkg1;

// cola circular 3

import static evaluacion.parcial.pkg1.Pila.cima;
import static evaluacion.parcial.pkg1.Pila.cimaaux;
import java.util.Random;

public class Cola{
	String[] ArregloCola;
        int primero=-1,ultimo=-1; //declaración cola principal
        String[] ArregloColaux; 
        int primeroaux=-1,ultimoaux=-1; //declarar cola auxiliar
	int Cant_elem=10;
        int Cant_elem2=10; //tamaño del arreglo cola
        
	//Constructor
	Cola(){
		ArregloCola=new String[Cant_elem];
                ArregloColaux =new String[Cant_elem2];
                primeroaux=-1;
		ultimoaux=-1;
		primero=-1;
		ultimo=-1;
                
                LlenarCola();
                
	}
        
    private void LlenarCola() 
    {
        Random random= new Random();
        int numero;
        
        for (int i = 0; i < 10; i++) {
            numero = random.nextInt((50 - 1) + 1 ) +1;
                Ingresar(String.valueOf(numero));
        }
        
         
    }


        public boolean ColaLlena(){
            //primero y último en los extremos
            if ((primero==0)&&(ultimo==Cant_elem-1)) 
                return(true);
            if (ultimo+1==primero) //último más uno alcanza a primero
                return (true);
            return(false);
        }

	//Encola un elemento
	public void Ingresar(String x)
        {
		if(VaciaCola()) 
                {
                        primero++;
			ultimo++;
                        //en la posición de último en ArregloCola se
                        //guarda el dato X
			ArregloCola[ultimo]=x; 
         	}
		else{
			if(ColaLlena())
				System.out.println("No hay campo");
			else
                            if (ultimo==Cant_elem-1)
                                ultimo=0;
                            else
                                ultimo++;
                        ArregloCola[ultimo]=x;
			}
	}
       

    //Desencola un elemento
	public String Avanzar(){
            String dato=null;
		if(VaciaCola())
			System.out.println("No hay Elementos");
		else{
                    dato=ArregloCola[primero];
                    if (primero == ultimo){
                        primero=-1;
                        ultimo=-1;
                        }
                    else{
                        if (primero==Cant_elem-1)
                            primero=0;
                        else
                            primero++;
                    }
		}
            return(dato);
	}

    public void Ingresaraux(String x)
        {
		if(ultimoaux==-1)
                {
			ultimoaux++;
                        primeroaux++;
			ArregloColaux[ultimoaux]=x;
		}
		else{
			ultimoaux++;
			if(ultimoaux==Cant_elem2)
				System.out.println("No hay campo");
			else
				ArregloColaux[ultimoaux]=x;
			}
	}

    public String Avanzaraux(){
            String dato=null;
		if(VaciaColaux())
			System.out.println("No hay Elementos");
		else{
                    dato=ArregloColaux[primeroaux];
                    if (primeroaux == ultimoaux){
                        primeroaux=-1;
                        ultimoaux=-1;
                        }
                    else{
                        primeroaux++;
                    }
		}
            return(dato);
	}

	//Retorna si esta vacia la cola
	public boolean VaciaColaux(){
		return (ultimoaux==-1 && primeroaux==-1);
	}

	//Retorna si esta vacia la cola 2
	public boolean VaciaCola(){
		return (ultimo==-1 && primero==-1);
	}
        
       public void Imprimir(){
        String desplazar2;
        if(VaciaCola())
            System.out.println("No hay Elementos");
        else{
            //System.out.println("La cola es: ");
            while(!VaciaCola()){
                desplazar2=Avanzar();
                
                Ingresaraux(desplazar2);
            }
            while(!VaciaColaux()){
                desplazar2=Avanzaraux();
                System.out.print(desplazar2+" - ");
                Ingresar(desplazar2);
            }
            System.out.println("");
	}
        
        
    }
       
       
    public boolean compararPila(Pila pila){
        boolean bandera = false;
        String desplazar2;
        if(VaciaCola())
            System.out.println("No hay Elementos");
        else{
            //System.out.println("La cola es: ");
            while(!VaciaCola()){
                desplazar2=Avanzar();
                Ingresaraux(desplazar2);
            }
            etiqueta_while: while(!VaciaColaux()){
                desplazar2=Avanzaraux();
                //System.out.print(desplazar2+" - ");
                
                //*********************
                String quedata,salida=" ";
                if (cima!=-1){ 
                    do {
                        quedata = pila.Pop();
                        if (quedata.equals(desplazar2)){
                            System.out.println(desplazar2 + " se compara"  + quedata);
                            System.out.println("iguales");
                            System.out.println("se termina el recorrido");
                            bandera = true;
                            break etiqueta_while;
                        }else{
                            System.out.println(desplazar2 + " se compara "  + quedata);
                            System.out.println("desiguales");
                        }
                            
                        salida=salida+quedata+" - "; //System.out.println mostrando
                        
                        pila.PushAux(quedata);            
                  }while(cima!=-1);
                  do {
                      quedata=pila.PopAux();
                      pila.Push(quedata);
                  }while(cimaaux!=-1);
                  //if(desplazar2.equals(salida))
                  //System.out.println(desplazar2 + " se compara"  + salida);
                  //JOptionPane.showMessageDialog(null, salida);
                }
                else {
                    System.out.println("La pila esta vacía");

                }
                
                
                //*********************
                
                
                
                Ingresar(desplazar2);
            }
            System.out.println("");
            
	}
        return bandera;
    }

}
		
