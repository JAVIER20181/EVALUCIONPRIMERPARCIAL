/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluacion.parcial.pkg1;

//package pilas;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//import java.io.*;
//package pilas;
import java.io.*;
import java.util.Random;
import javax.swing.JOptionPane;
/**
 *
 */
public class Pila {
    //Longitud maxima de la Pila
   public static final int MAX_LENGTH = 5;
   //Array que representa a la pila principal
   public static String Pila[] = new String[MAX_LENGTH];
   //Cima de la pila principal
   public static int cima = -1;
   //Array que representa a la pila auxiliar
   public static String Pilaaux[] = new String[MAX_LENGTH];
   //Cima de la pila auxiliar
   public static int cimaaux = -1;
   
   
   Pila()
   {
       LlenarPila();
   }
    

    public void Push(String dato)
    {
      if ((Pila.length-1)==cima){
        System.out.println("Capacidad de la pila al limite");
      }else{
            cima++;
            //System.out.println("Cima en la posición "+cima);
            Pila[cima]=dato;
      }
    }

    public void PushAux(String dato){
      if ((Pilaaux.length-1)==cimaaux){
        System.out.println("Capacidad de la pila auxiliar al limite");
      }else{
         cimaaux++;
         Pilaaux[cimaaux]=dato;
       }
    }

    public boolean VaciAaux(){
        return (cimaaux==-1);
    }

    public boolean Vacia(){
        if (cima==-1){
            return (true);
        }
        else {
            return(false);
        }
    }

    public String Pop(){
      String quedato;
      if(Vacia()){
          System.out.println("No se puede eliminar, pila vacía !!!" );
          return("");
      }else{
              quedato=Pila[cima];
	      Pila[cima] = null;
	      --cima;
              return(quedato);
            }
    }

    public String PopAux(){
      String quedato;
      if(cimaaux== -1){
            System.out.println("No se puede eliminar, pila vacía !!!" );
            return("");
      }else{
              quedato=Pilaaux[cimaaux];
	      Pilaaux[cimaaux] = null;
	      --cimaaux;
              return(quedato);
           }
    }
    
    private boolean PilaLlena()
    {
        if ((Pila.length-1)==cima)
            return true;
        else
            return false;
    }
    
    private boolean PilaLlenaAux()
    {
        if ((Pilaaux.length-1)==cimaaux)
            return true;
        else
            return false;
    }
    
    private void LlenarPila()
    {
        Random random= new Random();
        int numero;
        do {
           numero = random.nextInt((50 - 1) + 1 ) +1;
                Push(String.valueOf(numero));
            
        }while(!PilaLlena());
    }

public void Imprimir(){
      String quedata,salida=" ";
      if (cima!=-1)
      { 
          
          do {
            quedata = Pop();
            salida=salida+quedata+" - "; //System.out.println mostrando
            PushAux(quedata);            
        }while(cima!=-1);
        do {
            quedata=PopAux();
            Push(quedata);
        }while(cimaaux!=-1);
        System.out.println(salida);
        //JOptionPane.showMessageDialog(null, salida);
      }
      else {
          System.out.println("La pila esta vacía");
        
      }
    }
    
}